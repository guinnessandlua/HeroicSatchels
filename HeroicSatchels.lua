--------------------------------------------------------------------------------
--  File: HeroicSatchels.lua
--
--  HeroicSatchels License
--  This work is licensed under the Creative Commons Attribution-NonCommercial
--  4.0 International License. To view a copy of this license, visit
--  http://creativecommons.org/licenses/by-nc/4.0/.
--------------------------------------------------------------------------------
local data
local font = "Interface\\AddOns\\HeroicSatchels\\HeroicSatchels.TTF"
local hsTankTexture = 133652 --Bag icon
local hsHealerTexture = 133653 --Bag icon
local hsDPSTexture = 133655 --Bag icon


--Create Frames-----------------------------------------------------------------
    local function JoinTankQueue()
        --SetLFGRoles(#, #, #, #)
        --With the numbers being 4 boolean values: isLeader | isTank | isHealer | isDPS
        print("Tank Queue")
        SetLFGRoles(false, true, false, false)
        SetLFGDungeon(LE_LFG_CATEGORY_LFD, 1046)
        JoinLFG(LE_LFG_CATEGORY_LFD)
    end

    local function JoinHealerQueue()
        --SetLFGRoles(#, #, #, #)
        --With the numbers being 4 boolean values: isLeader | isTank | isHealer | isDPS
        print("Healer Queue")
        SetLFGRoles(false, false, true, false)
        SetLFGDungeon(LE_LFG_CATEGORY_LFD, 1046)
        JoinLFG(LE_LFG_CATEGORY_LFD)
    end

    local function JoinDPSQueue()
        --SetLFGRoles(#, #, #, #)
        --With the numbers being 4 boolean values: isLeader | isTank | isHealer | isDPS
        print("DPS Queue")
        SetLFGRoles(false, false, false, true)
        SetLFGDungeon(LE_LFG_CATEGORY_LFD, 1046)
        JoinLFG(LE_LFG_CATEGORY_LFD)
    end

    local function HeroicSatchelsFramesInit()
        --Tank
        local tank = { }
        tank.point  = data.tankIcon.point  or "CENTER"
        tank.rPoint = data.tankIcon.rPoint or "CENTER"
        tank.x      = data.tankIcon.x      or 0
        tank.y      = data.tankIcon.y      or 0

        hsTankFrame = hsTankFrame or CreateFrame("button", "hsTankFrame", UIParent)
        hsTankFrame:SetSize(64, 64)
        hsTankFrame:SetPoint(tank.point, UIParent, tank.rPoint, tank.x, tank.y)
        hsTankFrame.texture = hsTankFrame:CreateTexture(nil, "BACKGROUND")
        hsTankFrame.texture:SetAllPoints(true)
        hsTankFrame.texture:SetTexture(hsTankTexture)
        hsTankFrame.texture:SetTexCoord(.08, .92, .08, .92)
        hsTankFrame:SetMovable(false)
        hsTankFrame:EnableMouse(false)
        hsTankFrame:RegisterForDrag("LeftButton")
        hsTankFrame:SetScript("OnDragStart", hsTankFrame.StartMoving)
        hsTankFrame:SetScript("OnDragStop", hsTankFrame.StopMovingOrSizing)
        hsTankFrame.Text = hsTankFrame:CreateFontString("hsTankFrameText", ARTWORK, "GameFontNormal")
        hsTankFrame.Text:SetPoint("CENTER", 0, -20)
        hsTankFrame.Text:SetSize(64, 64)
        hsTankFrame.Text:SetTextColor(1, 1, 1, 0.75)
        hsTankFrame.Text:SetFont(font, 15, "OUTLINE")
        hsTankFrame.Text:SetJustifyH("CENTER")
        hsTankFrame.Text:SetText("TANK")
        hsTankFrame:SetScript("OnClick", function(self, button) JoinTankQueue() end)
        hsTankFrame:SetMovable(false)
        hsTankFrame:EnableMouse(true)

        hsTankFrame:Hide()

        --Heals
        local healer = { }
        healer.point  = data.healerIcon.point  or "CENTER"
        healer.rPoint = data.healerIcon.rPoint or "CENTER"
        healer.x      = data.healerIcon.x      or 100
        healer.y      = data.healerIcon.y      or 0

        hsHealerFrame = hsHealerFrame or CreateFrame("button", "hsHealerFrame", UIParent)
        hsHealerFrame:SetSize(64, 64)
        hsHealerFrame:SetPoint(healer.point, UIParent, healer.rPoint, healer.x, healer.y)
        hsHealerFrame.texture = hsHealerFrame:CreateTexture(nil, "BACKGROUND")
        hsHealerFrame.texture:SetAllPoints(true)
        hsHealerFrame.texture:SetTexture(hsHealerTexture)
        hsHealerFrame.texture:SetTexCoord(.08, .92, .08, .92)
        hsHealerFrame:SetMovable(false)
        hsHealerFrame:EnableMouse(false)
        hsHealerFrame:RegisterForDrag("LeftButton")
        hsHealerFrame:SetScript("OnDragStart", hsHealerFrame.StartMoving)
        hsHealerFrame:SetScript("OnDragStop", hsHealerFrame.StopMovingOrSizing)
        hsHealerFrame.Text = hsHealerFrame:CreateFontString("hsHealerFrameText", ARTWORK, "GameFontNormal")
        hsHealerFrame.Text:SetPoint("CENTER", 0, -20)
        hsHealerFrame.Text:SetSize(64, 64)
        hsHealerFrame.Text:SetTextColor(1, 1, 1, 0.75)
        hsHealerFrame.Text:SetFont(font, 15, "OUTLINE")
        hsHealerFrame.Text:SetJustifyH("CENTER")
        hsHealerFrame.Text:SetText("HEALER")
        hsHealerFrame:SetScript("OnClick", function(self, button) JoinHealerQueue() end)
        hsHealerFrame:SetMovable(false)
        hsHealerFrame:EnableMouse(true)

        hsHealerFrame:Hide()

        --DPS
        local dps = { }
        dps.point  = data.dpsIcon.point  or "CENTER"
        dps.rPoint = data.dpsIcon.rPoint or "CENTER"
        dps.x      = data.dpsIcon.x      or 200
        dps.y      = data.dpsIcon.y      or 0

        hsDPSFrame = hsDPSFrame or CreateFrame("button", "hsDPSFrame", UIParent)
        hsDPSFrame:SetSize(64, 64)
        hsDPSFrame:SetPoint(dps.point, UIParent, dps.rPoint, dps.x, dps.y)
        hsDPSFrame.texture = hsDPSFrame:CreateTexture(nil, "BACKGROUND")
        hsDPSFrame.texture:SetAllPoints(true)
        hsDPSFrame.texture:SetTexture(hsDPSTexture)
        hsDPSFrame.texture:SetTexCoord(.08, .92, .08, .92)
        hsDPSFrame:SetMovable(false)
        hsDPSFrame:EnableMouse(false)
        hsDPSFrame:RegisterForDrag("LeftButton")
        hsDPSFrame:SetScript("OnDragStart", hsDPSFrame.StartMoving)
        hsDPSFrame:SetScript("OnDragStop", hsDPSFrame.StopMovingOrSizing)
        hsDPSFrame.Text = hsDPSFrame:CreateFontString("hsDPSFrameText", ARTWORK, "GameFontNormal")
        hsDPSFrame.Text:SetPoint("CENTER", -1, -21)
        hsDPSFrame.Text:SetSize(62, 12)
        hsDPSFrame.Text:SetTextColor(1, 1, 1, 0.75)
        hsDPSFrame.Text:SetFont(font, 15, "OUTLINE")
        hsDPSFrame.Text:SetJustifyH("CENTER")
        hsDPSFrame.Text:SetText("DPS")
        hsDPSFrame:SetScript("OnClick", function(self, button) JoinDPSQueue() end)
        hsDPSFrame:SetMovable(false)
        hsDPSFrame:EnableMouse(true)

        hsDPSFrame:Hide()
    end


--Helper Functions--------------------------------------------------------------
    local function Round(number, decimals)
        if decimals == nil then decimals = 0 end

        local rounded = (("%%.%df"):format(decimals)):format(number)

        return tonumber(rounded)
    end

    local function SavePositions()
        local point, relativeTo, relativePoint, xOfs, yOfs = hsTankFrame:GetPoint(1)
        data.tankIcon["point"]         = point
        data.tankIcon["relativeTo"]    = relativeTo:GetName()
        data.tankIcon["relativePoint"] = relativePoint
        data.tankIcon["xOfs"]          = Round(xOfs)
        data.tankIcon["yOfs"]          = Round(yOfs)

        point, relativeTo, relativePoint, xOfs, yOfs = hsTankFrame:GetPoint(2)
        if point then
            data.tankIcon["point2"]         = point
            data.tankIcon["relativeTo2"]    = relativeTo:GetName()
            data.tankIcon["relativePoint2"] = relativePoint
            data.tankIcon["xOfs2"]          = Round(xOfs)
            data.tankIcon["yOfs2"]          = Round(yOfs)
        end

        point, relativeTo, relativePoint, xOfs, yOfs = hsHealerFrame:GetPoint(1)
        data.healerIcon["point"]         = point
        data.healerIcon["relativeTo"]    = relativeTo:GetName()
        data.healerIcon["relativePoint"] = relativePoint
        data.healerIcon["xOfs"]          = Round(xOfs)
        data.healerIcon["yOfs"]          = Round(yOfs)

        point, relativeTo, relativePoint, xOfs, yOfs = hsHealerFrame:GetPoint(2)
        if point then
            data.healerIcon["point2"]         = point
            data.healerIcon["relativeTo2"]    = relativeTo:GetName()
            data.healerIcon["relativePoint2"] = relativePoint
            data.healerIcon["xOfs2"]          = Round(xOfs)
            data.healerIcon["yOfs2"]          = Round(yOfs)
        end

        point, relativeTo, relativePoint, xOfs, yOfs = hsDPSFrame:GetPoint(1)
        data.dpsIcon["point"]         = point
        data.dpsIcon["relativeTo"]    = relativeTo:GetName()
        data.dpsIcon["relativePoint"] = relativePoint
        data.dpsIcon["xOfs"]          = Round(xOfs)
        data.dpsIcon["yOfs"]          = Round(yOfs)

        point, relativeTo, relativePoint, xOfs, yOfs = hsDPSFrame:GetPoint(2)
        if point then
            data.dpsIcon["point2"]         = point
            data.dpsIcon["relativeTo2"]    = relativeTo:GetName()
            data.dpsIcon["relativePoint2"] = relativePoint
            data.dpsIcon["xOfs2"]          = Round(xOfs)
            data.dpsIcon["yOfs2"]          = Round(yOfs)
        end
    end

    local function SavedVariablesInit()
        --Check if we have saved variable data that we can restore.
        if not not HSdata then
            data = HSdata
        --Otherwise start fresh.
        else
            data = { }
            data.tankIcon = { }
            data.healerIcon = { }
            data.dpsIcon = { }
        end

        --Push data to saved variables if saved variables are not set.
        if not HSdata then
            HSdata = data
        end
    end


--Timer-------------------------------------------------------------------------
    local function Timer()
        --Check for Satchels
        hsEligible, hsForTank, hsForHealer, hsForDamage, hsItemCount, hsMoney, hsXP = GetLFGRoleShortageRewards(1046, 1)
        --print(GetTime(), hsEligible, hsForTank, hsForHealer, hsForDamage, hsItemCount, hsMoney, hsXP)

        --Instance check
        local inInstance, _ = IsInInstance()

        if inInstance then
            RA.Log("in instance: hide all frames")
            if hsTankFrame:IsVisible() then
                hsTankFrame:Hide()
            end            

            if hsHealerFrame:IsVisible() then
                hsHealerFrame:Hide()
            end            

            if hsDPSFrame:IsVisible() then
                hsDPSFrame:Hide()
            end
        else
            RA.Log("not in instance")
            if hsEligible then
                --Tank
                if hsForTank then
                    RA.Log("eligible: show tank frame")
                    hsTankFrame:Show()
                else
                    RA.Log("eligible: hide tank frame")
                    hsTankFrame:Hide()
                end
                
                --Heal
                if hsForHealer then
                    RA.Log("eligible: show healer frame")
                    hsHealerFrame:Show()
                else
                    RA.Log("eligible: hide healer frame")
                    hsHealerFrame:Hide()
                end
                
                --DPS
                if hsForDamage then
                    RA.Log("eligible: show dps frame")
                    hsDPSFrame:Show()
                else
                    RA.Log("eligible: hide dps frame")
                    hsDPSFrame:Hide()
                end
            else
                RA.Log("not eligible: hide all frames")
                hsTankFrame:Hide()
                hsHealerFrame:Hide()
                hsDPSFrame:Hide()
            end            
        end
    end
--------------------------------------------------------------------------------


--Commands----------------------------------------------------------------------
    local slashCommands = {
        ["lock"] = function(more)
            --Tank
            hsTankFrame:Hide()
            hsTankFrame:SetMovable(false)
            hsTankFrame:EnableMouse(true)

            --Lock in new X and Y
            local point, _, relativePoint, xOfs, yOfs = hsTankFrame:GetPoint(1)
            data.tankIcon.point  = point
            data.tankIcon.rPoint = relativePoint
            data.tankIcon.x      = Round(xOfs)
            data.tankIcon.y      = Round(yOfs)

            --Healer
            hsHealerFrame:Hide()
            hsHealerFrame:SetMovable(false)
            hsHealerFrame:EnableMouse(true)

            --Lock in new X and Y
            local point, _, relativePoint, xOfs, yOfs = hsHealerFrame:GetPoint(1)
            data.healerIcon.point  = point
            data.healerIcon.rPoint = relativePoint
            data.healerIcon.x      = Round(xOfs)
            data.healerIcon.y      = Round(yOfs)

            --DPS
            hsDPSFrame:Hide()
            hsDPSFrame:SetMovable(false)
            hsDPSFrame:EnableMouse(true)

            --Lock in new X and Y
            local point, _, relativePoint, xOfs, yOfs = hsDPSFrame:GetPoint(1)
            data.dpsIcon.point  = point
            data.dpsIcon.rPoint = relativePoint
            data.dpsIcon.x      = Round(xOfs)
            data.dpsIcon.y      = Round(yOfs)

            --Reset timer
            heroicSatchelsTimer:Cancel()
            C_Timer.NewTicker(5, Timer)
        end,

        ["unlock"] = function(more)
            heroicSatchelsTimer:Cancel()

            --Tank
            hsTankFrame:Show()
            hsTankFrame:SetMovable(true)
            hsTankFrame:EnableMouse(true)

            --Healer
            hsHealerFrame:Show()
            hsHealerFrame:SetMovable(true)
            hsHealerFrame:EnableMouse(true)

            --DPS
            hsDPSFrame:Show()
            hsDPSFrame:SetMovable(true)
            hsDPSFrame:EnableMouse(true)
        end,

        ["movetank"] = function(more)
            if more ~= "" then
                local x, y = more:match("([^,]+),([^,]+)")
                hsTankFrame:SetPoint("CENTER", WorldFrame, "CENTER", x, y)

                --Lock in new X and Y
                local point, _, relativePoint, xOfs, yOfs = hsTankFrame:GetPoint(1)
                data.tankIcon.point  = point
                data.tankIcon.rPoint = relativePoint
                data.tankIcon.x      = Round(xOfs)
                data.tankIcon.y      = Round(yOfs)
            else
                heroicSatchelsTimer:Cancel()
                hsTankFrame:Show()
                hsTankFrame:SetMovable(true)
                hsTankFrame:EnableMouse(true)
            end
        end,

        ["movehealer"] = function(more)
            if more ~= "" then
                local x, y = more:match("([^,]+),([^,]+)")
                hsHealerFrame:SetPoint("CENTER", WorldFrame, "CENTER", x, y)

                --Lock in new X and Y
                local point, _, relativePoint, xOfs, yOfs = hsHealerFrame:GetPoint(1)
                data.healerIcon.point  = point
                data.healerIcon.rPoint = relativePoint
                data.healerIcon.x      = Round(xOfs)
                data.healerIcon.y      = Round(yOfs)
            else
                heroicSatchelsTimer:Cancel()
                hsHealerFrame:Show()
                hsHealerFrame:SetMovable(true)
                hsHealerFrame:EnableMouse(true)
            end
        end,

        ["movedps"] = function(more)
            if more ~= "" then
                local x, y = more:match("([^,]+),([^,]+)")
                hsDPSFrame:SetPoint("CENTER", WorldFrame, "CENTER", x, y)

                --Lock in new X and Y
                local point, _, relativePoint, xOfs, yOfs = hsDPSFrame:GetPoint(1)
                data.dpsIcon.point  = point
                data.dpsIcon.rPoint = relativePoint
                data.dpsIcon.x      = Round(xOfs)
                data.dpsIcon.y      = Round(yOfs)
            else
                heroicSatchelsTimer:Cancel()
                hsDPSFrame:Show()
                hsDPSFrame:SetMovable(true)
                hsDPSFrame:EnableMouse(true)
            end
        end,

        ["reset"] = function(more)
            --Tank
            hsTankFrame:ClearAllPoints();
            hsTankFrame:SetPoint("CENTER", WorldFrame, "CENTER", 0, 0)

            --Lock in new X and Y
            local point, _, relativePoint, xOfs, yOfs = hsTankFrame:GetPoint(1)
            data.icon.point  = point
            data.icon.rPoint = relativePoint
            data.icon.x      = Round(xOfs)
            data.icon.y      = Round(yOfs)

            --Healer
            hsHealerFrame:ClearAllPoints();
            hsHealerFrame:SetPoint("CENTER", WorldFrame, "CENTER", 0, 0)

            --Lock in new X and Y
            local point, _, relativePoint, xOfs, yOfs = hsHealerFrame:GetPoint(1)
            data.healerIcon.point  = point
            data.healerIcon.rPoint = relativePoint
            data.healerIcon.x      = Round(xOfs)
            data.healerIcon.y      = Round(yOfs)

            --DPS
            hsDPSFrame:ClearAllPoints();
            hsDPSFrame:SetPoint("CENTER", WorldFrame, "CENTER", 0, 0)

            --Lock in new X and Y
            local point, _, relativePoint, xOfs, yOfs = hsDPSFrame:GetPoint(1)
            data.dpsIcon.point  = point
            data.dpsIcon.rPoint = relativePoint
            data.dpsIcon.x      = Round(xOfs)
            data.dpsIcon.y      = Round(yOfs)
        end,

    }
    _G["SLASH_SS1"] = "/hs"
    _G["SLASH_SS2"] = "/heroicsatchels"
    function SlashCmdList.SS(cmd, editBox)
        local msg, more = cmd:match("^(%S*)%s*(.-)$")

        if msg == "" then
            print("HeroicSatchels version: "..GetAddOnMetadata("HeroicSatchels", "Version").."")
            print("Options: lock | unlock | movetank | movehealer | movedps | reset")

        else
            if slashCommands[msg] then
                slashCommands[msg](more)
            else
                print("HeroicSatchels: unknown command")
            end
        end
    end
--------------------------------------------------------------------------------


--Events------------------------------------------------------------------------
    local eventsFrame, events = CreateFrame("Frame"), {}

    function events:PLAYER_ENTERING_WORLD(...)
        --Saved variables set/check/initialize
        SavedVariablesInit()

        --Icon frame Initialize
        HeroicSatchelsFramesInit()

        --Timer
        heroicSatchelsTimer = heroicSatchelsTimer or C_Timer.NewTicker(5, Timer)
    end

    function events:PLAYER_LOGOUT(...)
        SavePositions()

        if data then
            _G["HSdata"] = data
        end
    end

    eventsFrame:SetScript("OnEvent", function(self, event, ...)
        events[event](self, ...)
        --
    end)

    -- Register all events for which handlers have been defined
    for k, v in pairs(events) do
        eventsFrame:RegisterEvent(k)
        --
    end
--------------------------------------------------------------------------------